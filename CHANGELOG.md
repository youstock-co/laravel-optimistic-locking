# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/xint0-open-source/laravel-optimistic-locking/compare/1.3.5...master)

## [1.3.5](https://gitlab.com/xint0-open-source/laravel-optimistic-locking/compare/1.3.4...1.3.5) 2024-06-28

### Changed

- Add support for Laravel 11.

## [1.3.4](https://gitlab.com/xint0-open-source/laravel-optimistic-locking/compare/1.3.3...1.3.4) 2023-08-11

### Changed

- Add support for Laravel 10.

## [1.3.3](https://gitlab.com/xint0-open-source/laravel-optimistic-locking/compare/1.3.2...1.3.3) 2022-12-13

### Changed

- Upgrade dependencies.

## [1.3.2](https://gitlab.com/xint0-open-source/laravel-optimistic-locking/compare/1.3.1...1.3.2) 2022-10-16

### Fixed

- Package name in installation instructions.
- License link.

### Changed

- Update project location.

## [1.3.1](https://gitlab.com/xint0-open-source/laravel-optimistic-locking/compare/1.3.0...1.3.1) 2021-04-12

### Fixed

- Ensure sync changes before fire updated event.

## [1.3.0](https://gitlab.com/xint0-open-source/laravel-optimistic-locking/compare/1.2.2...1.3.0) 2021-03-08

### Added

- Throw `StaleModelLockingException` on delete of a stale model. #3