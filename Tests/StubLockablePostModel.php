<?php

namespace Reshadman\OptimisticLocking\Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Reshadman\OptimisticLocking\OptimisticLocking;

/**
 * Class StubLockablePostModel
 *
 * @package Reshadman\OptimisticLocking\Tests
 *
 * @property Carbon|null $created_at
 * @property string|null $description
 * @property int|null $id
 * @property int|null $lock_version
 * @property string|null $title
 * @property Carbon|null $updated_at
 * @property int|null $user_id
 */
class StubLockablePostModel extends Model
{
    use OptimisticLocking;

    protected $guarded = [];

    protected $table = 'posts';
}