<?php

declare(strict_types=1);

namespace Reshadman\OptimisticLocking;

use Illuminate\Database\Eloquent\Model;

class StaleModelLockingException extends \RuntimeException
{
    protected function __construct(
        $message,
        public Model $model,
        $code = 0,
        ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function createFromContext(
        string $modelAction,
        Model $model,
        $code = 0,
        ?Throwable $previous = null
    ): self {
        $primaryKey = $model->getKey();
        if(!is_string($primaryKey) && !is_int($primaryKey)) {
            $primaryKey = json_encode($primaryKey);
        }

        return new self(
            message: sprintf(
                'Model %s (id %s) has been changed during %s.',
                $model::class,
                $primaryKey,
                $modelAction,
            ),
            model: $model,
            code: $code,
            previous: $previous
        );
    }
}